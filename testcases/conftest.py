import pytest
from utils.data import load_yaml
from utils.db import DB, FuelCardDB
from utils.http import HTTP, FuelCardApi


@pytest.fixture(scope='session')
def load():
    return load_yaml  # 返回一个函数，可以在用例中传入不同的参数获取不同的数据


@pytest.fixture(scope='session')
def data():  # 获取默认全部数据
    return load_yaml('data.yaml')


@pytest.fixture
def test_data(request):  # 获取默认数据中 本用例数据
    data = load_yaml('data.yaml')
    case_name = request.function.__name__  # 获取调用函数名称
    return data.get(case_name)
    # print(dir(request))

@pytest.fixture(scope='session')
def db():
    db = FuelCardDB()
    yield db
    db.close()



@pytest.fixture(scope='session')
def http(base_url):
    return HTTP(base_url)


@pytest.fixture(scope='session')
def api(base_url):
    return FuelCardApi(base_url)