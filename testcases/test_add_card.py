import random
import pytest


def test_add_card(api, data, db):
    """接口、参数正确"""
    card_number = data.get('new_card_001')
    # 数据检查或数据准备
    db.delete_card(card_number)
    res_dict = api.add_card(card_number)
    # !!!数据清理
    db.delete_card(card_number)
    assert res_dict['code'] == 200
    assert res_dict['msg'] == '添加卡成功'
    # assert res_dict['success'] is True  # bug


def test_add_card_twice(api, data, db):
    card_number = data['new_card_002']
    db.delete_card(card_number)  # 数据准备，确保为新卡
    res = api.add_card(card_number)
    res2 = api.add_card(card_number)
    assert res2.get('code') == 5000
    db.delete_card(card_number)  # 清理数据


def test_add_two_card(api, data, db):
    card_number1 = data['new_card_001']
    card_number2 = data['new_card_002']
    # [delete_card(card_number) for card_number in [card_number1, card_number2]]
    db.delete_card(card_number1)
    db.delete_card(card_number2)

    res = api.add_card(card_number1)
    res2 = api.add_card(card_number2)
    db.delete_card(card_number1)
    db.delete_card(card_number2)
    assert res2['code'] == 200