import logging
import os
from datetime import datetime


base_dir = os.path.dirname(os.path.dirname(__file__))


def get_logger(name):
    today = datetime.now().strftime('%Y%m%d')
    logger = logging.Logger(name)
    logger.setLevel(logging.DEBUG)

    hander1 = logging.StreamHandler()  # 输出到屏幕
    # hander1.setLevel(logging.INFO)
    hander2 = logging.FileHandler(
        filename=os.path.join(base_dir, 'logs', f'{today}.log'),
        encoding='utf-8',
        mode='a'
    )
    fmt = logging.Formatter(
        '%(asctime)s %(name)s %(levelname)s: %(message)s'
    )
    hander1.setFormatter(fmt)
    hander2.setFormatter(fmt)

    logger.addHandler(hander1)
    logger.addHandler(hander2)
    return logger


if __name__ == '__main__':
    log = get_logger('utils')
    log.debug('调试信息')
    log.info('正式信息')