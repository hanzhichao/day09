"""操作数据库"""
import os

import pymysql

from utils.log import get_logger

log = get_logger('db')


DB_CONF = dict(
    host=os.getenv('MYSQL_HOST'),
    port=3306,
    user=os.getenv('MYSQL_USER'),
    password=os.getenv('MYSQL_PWD'),
    db=os.getenv('MYSQL_DB'),
    autocommit=True
)


class DB(object):
    def __init__(self, db_conf=DB_CONF):
        self.conn = pymysql.connect(**db_conf)
        self.cur = self.conn.cursor(pymysql.cursors.DictCursor)

    def query(self, sql):
        self.cur.execute(sql)
        result = self.cur.fetchall()
        log.debug(f'查询sql: {sql} 查询结果: {result}')
        return result

    def change_db(self, sql):
        self.cur.execute(sql)
        log.debug(f'执行sql: {sql}')

    def close(self):
        log.debug('关闭数据库连接')
        self.cur.close()
        self.conn.close()



class FuelCardDB(DB):
    def check_card(self, card_number):
        result = self.query(f"SELECT id FROM cardinfo WHERE cardNumber='{card_number}';")
        if result:
            return True
        return False
        # return True if result else False

    def delete_card(self, card_number):
        if self.check_card(card_number):
            self.change_db(f"DELETE FROM cardinfo WHERE cardNumber='{card_number}'")


if __name__ == '__main__':  # 模块私有代码，一般用于本模块调试，其他模块调用时不会执行
    db = DB()
    sql = 'select * from carduser where userId=888889'
    print(db.query(sql))

