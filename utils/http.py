import requests
from utils.log import get_logger

log = get_logger('http')


class HTTP(object):
    def __init__(self, base_url=None):
        self.session = requests.session()
        self.session.headers = {
            'Token': 'abc123'
        }
        self.session.timeout=10
        self.base_url = base_url

    def request(self, method, url: str, **kwargs):
        if self.base_url and not url.startswith('http'):
            url = self.base_url + url
        res = self.session.request(method, url, **kwargs)
        log.debug(f'发送请求 {method} {url} 状态码 {res.status_code}')
        return res

    def get(self, url, **kwargs):
        return self.request('GET', url, **kwargs)

    def post(self, url, **kwargs):
        return self.request('POST', url, **kwargs)


class FuelCardApi(HTTP):
    def add_card(self, card_number):
        url = '/gasStation/process'
        data = {
            "dataSourceId": "bHRz",
            "methodId": "00A",
            "CardInfo": {
                "cardNumber": card_number
            }
        }
        res = self.post(url, json=data)
        return res.json()