"""发送邮件等通知"""
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
import os

from utils.log import get_logger

log = get_logger('notify')

SMTP_USER = os.getenv('SMTP_USER')
SMTP_PWD = os.getenv('SMTP_PWD')




def send_mail(subject, body, receivers:list, attachments=None):
    msg = MIMEMultipart()
    msg.attach(MIMEText(body, 'html' ,'utf-8'))

    if attachments:
        for file_path in attachments:
            file_name = file_path.split(os.path.sep)[-1]  # os.path.sep  获取到当前系统路径的分隔符
            att = MIMEText(open(file_path, 'rb').read(), 'base64', 'utf-8')
            att['Content-Type'] = 'application/octet-stream'
            att['Content-Disposition'] = f'attachment; filename={file_name}'
            msg.attach(att)

    msg['Subject'] = subject
    msg['From'] = SMTP_USER
    msg['To'] = ','.join(receivers)

    smtp = smtplib.SMTP_SSL('smtp.sina.com')
    smtp.login(SMTP_USER, SMTP_PWD)
    for user in receivers:
        smtp.sendmail(SMTP_USER, user, msg.as_string())
    log.debug('发送邮件成功')